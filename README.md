## graphiql-deployment

Deployment for Timisoara version of [GraphiQL](https://github.com/graphql/graphiql). Production deployment available from https://api.opentransport.ro/graphiql/timisoara
