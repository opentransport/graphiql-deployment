#!/bin/bash
set -e

# This is run at ci, created an image that contains all the tools needed in
# databuild
#
# Set these environment variables
#DOCKER_USER // dockerhub credentials
#DOCKER_AUTH

ORG=${ORG:-opentransport}
DOCKER_TAG=${CI_COMMIT_SHORT_SHA:-latest}
DOCKER_IMAGE=$ORG/graphiql:${DOCKER_TAG}
LATEST_IMAGE=$ORG/graphiql:latest
PROD_IMAGE=$ORG/graphiql:prod


echo Building graphiql: $DOCKER_IMAGE

docker build  --tag=$DOCKER_IMAGE -f Dockerfile .

if [ -z ${CI_MERGE_REQUEST_ID} ]; then
  if [ "$CI_COMMIT_TAG" ];then
    echo "processing release $CI_COMMIT_TAG"
    #release do not rebuild, just tag
    docker pull ${DOCKER_IMAGE}
    docker tag ${DOCKER_IMAGE} ${PROD_IMAGE}
    docker login -u ${DOCKER_USER} -p ${DOCKER_AUTH}
    docker push ${PROD_IMAGE}
  else
    echo "processing master build $CI_COMMIT_SHORT_SHA"
    docker build  --tag=$DOCKER_IMAGE -f Dockerfile .
    docker login -u ${DOCKER_USER} -p ${DOCKER_AUTH}
    docker push ${DOCKER_IMAGE}
    docker tag ${DOCKER_IMAGE} ${LATEST_IMAGE}
    docker push ${LATEST_IMAGE}
  fi
else
  docker build  --tag=$DOCKER_IMAGE -f Dockerfile .
fi

echo Build completed
