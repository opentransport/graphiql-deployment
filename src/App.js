import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import GraphiQL from './GraphiQL'

const configs = [
  { title: 'Timisoara', router: 'timisoara' },
  { title: 'Romania', router: 'romania' }
]

export default class extends React.Component {
  render() { 
    return (
      <Router basename="/graphiql">
        <GraphiQL configs={configs}/>
      </Router>
    );
  }
}
