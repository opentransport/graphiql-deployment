FROM node:12-alpine as build
MAINTAINER OpenTransport version: 0.1

WORKDIR /opt/ot-graphql

COPY . ./
RUN yarn && yarn build

FROM node:12-alpine
WORKDIR /app
COPY --from=build /opt/ot-graphql/run.sh ./
COPY --from=build /opt/ot-graphql/build ./
COPY --from=build /opt/ot-graphql/serve.json ./
RUN yarn global add serve
EXPOSE 8080
ENTRYPOINT ["./run.sh"]
